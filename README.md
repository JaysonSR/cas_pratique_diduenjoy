### Cas pratique

**Titre du cas pratique :** Automatisation de la création de données à partir de fichiers Excel avec Ruby.

**Contexte :** Votre entreprise reçoit régulièrement des fichiers Excel contenant des données clients et vous devez créer ces données dans la base de données de l'entreprise. La création de ces données est aujourd’hui faite de façon manuelle, ce qui est fastidieux et peut entraîner des erreurs. Vous avez été chargé d'automatiser ce processus en écrivant des scripts Ruby.

**Objectifs :**

- Comprendre le format des fichiers Excel et la structure des données qu'ils contiennent.
- Écrire un script Ruby pour extraire les données des fichiers Excel et les stocker dans des variables Ruby.
- Afficher les données en console de manière structuré.
- Compléter le script Ruby pour stocker en base de donnée les données stockées dans les variables Ruby.