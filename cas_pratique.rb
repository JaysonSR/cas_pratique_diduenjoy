require 'roo'
require 'pg'

connection = PG.connect(dbname: 'due', user: 'due', password: 'due', host: 'localhost')

connection.exec("TRUNCATE TABLE public.orders")
connection.exec("TRUNCATE TABLE public.packages")
connection.exec("TRUNCATE TABLE public.items")


xlsx = Roo::Excelx.new('Orders.xlsx')

xlsx.each_with_pagename do |name, sheet|
    put sheet.row(1)
end

id_sheet = 0
xlsx.sheets.each do |sheet_name|
    id_sheet = id_sheet + 1
    order = []
    puts "\n \n NEW ORDER \n \n \n"
    package = []
    id_package = 0
    item = []
    id_item = 0

    connection.exec_params("INSERT INTO public.orders (orderid, odername) VALUES ($1, $2)", [id_sheet, sheet_name])
    connection.exec_params("INSERT INTO public.packages (packageid, orderid) VALUES ($1, $2)", [id_package,id_sheet])


    sheet = xlsx.sheet(sheet_name)
    sheet.each_row_streaming(offset: 1) do |row|
        row_data = []
        row.each_with_index do |cell, i|
            if cell.class == Roo::Excelx::Cell::Empty
                row_data << nil
            end
            row_data << cell
        end
        
        if id_package == row_data[0].to_s.to_i && id_item == row_data[1].to_s.to_i
            item << row_data[2]
            item << row_data[3]
        else
            id_item = row_data[1].to_s.to_i

            db_item = Array.new()

            for i in (0..item.size).step(2)
                if item[i].to_s == "name"
                    db_item[0] = item[i+1]
                elsif item[i].to_s == "price"
                    db_item[1] = item[i+1]
                elsif item[i].to_s == "ref"
                    db_item[2] = item[i+1]
                elsif item[i].to_s == "warranty"
                    db_item[3] = item[i+1]
                elsif item[i].to_s == "duration"
                    db_item[4] = item[i+1]
                end
            end


            package << db_item
            
            item = []
            item << row_data[2]
            item << row_data[3]
        end

        if id_package != row_data[0].to_s.to_i
            order << package
            package = []
            id_package = row_data[0].to_s.to_i

            connection.exec_params("INSERT INTO public.packages (packageid, orderid) VALUES ($1, $2)", [id_package,id_sheet])
        end
    end

    db_item = Array.new()

    for i in (0..item.size).step(2)
        if item[i].to_s == "name"
            db_item[0] = item[i+1]
        elsif item[i].to_s == "price"
            db_item[1] = item[i+1]
        elsif item[i].to_s == "ref"
            db_item[2] = item[i+1]
        elsif item[i].to_s == "warranty"
            db_item[3] = item[i+1]
        elsif item[i].to_s == "duration"
            db_item[4] = item[i+1]
        end
    end

    package << db_item

    order << package

    pck = 0
    ite = 0
    
    order.each do |pack|    
        puts "\n ---NEW PACKAGE--- \n\n"
        pack.each do |it|
            puts it
            puts "------------------------------"

            connection.exec_params("INSERT INTO public.items (itemid, name, price, ref, packageid, warranty, duration) VALUES ($1, $2, $3, $4, $5, $6, $7)", [ite, it[0], it[1], it[2], pck, it[3], it[4]])
            
            ite = ite + 1
        end
        pck = pck + 1
        ite = 0
    end            
end

connection.close